from django.shortcuts import render

posts_list = [
    {
        'pk':1,
        'title':'title 1',
        'description':'post 1 description 1',
        'body':'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
        'image_url':'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.freepik.com%2Ffree-photos-vectors%2Fbackground&psig=AOvVaw35XBOZjR_FygYSLrH81mQT&ust=1707410686991000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCMj4287WmYQDFQAAAAAdAAAAABAE'
    },
    {
        'pk':2,
        'title': 'title 2',
        'description':'post 2 description 2',
        'body':'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
        'image_url':'https://www.google.com/url?sa=i&url=https%3A%2F%2Fpixabay.com%2Fimages%2Fsearch%2Fnature%2F&psig=AOvVaw35XBOZjR_FygYSLrH81mQT&ust=1707410686991000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCMj4287WmYQDFQAAAAAdAAAAABAI'
    },
    {
        'pk':3,
        'title': 'title 3',
        'description':'post 3 description 3',
        'body':'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
        'image_url':'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.freepik.com%2Ffree-photos-vectors%2Fbackground&psig=AOvVaw35XBOZjR_FygYSLrH81mQT&ust=1707410686991000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCMj4287WmYQDFQAAAAAdAAAAABAQ'
    },
    {
        'pk':4,
        'title': 'title 4',
        'description':'post 4 description 4',
        'body':'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
        'image_url':'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.shutterstock.com%2Fexplore%2Froyalty-free-images&psig=AOvVaw35XBOZjR_FygYSLrH81mQT&ust=1707410686991000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCMj4287WmYQDFQAAAAAdAAAAABAY'
    }, 
    {
        'pk':5,
        'title': 'title 5',
        'description':'post 5 description 5',
        'body':'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
        'image_url':'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.dreamstime.com%2Fphotos-images%2Fnature.html&psig=AOvVaw35XBOZjR_FygYSLrH81mQT&ust=1707410686991000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCMj4287WmYQDFQAAAAAdAAAAABAw'
    }
]



def index_view(request) :
    return render(request,'index.html',
                    context= {'posts': posts_list} )
def post_view(request, pk):
    context_post = {}
    for post in posts_list:
        p = str(post.get('pk'))
        if str(post.get('pk')) == pk:
            context_post = post

    print(context_post)

    return render(request, 'post_view.html', 
                  context={'post': context_post})
